# Minh Tan Pham A01215507

# Configure the AWS Provider
provider "aws" {
  region = "us-west-2"
}

# Variable for vpc cidr block
variable "base_cidr_block" {
  description = "default cidr block"
  default = "192.168.0.0/16"
}


# Create a VPC
resource "aws_vpc" "a02_vpc" {
  cidr_block = var.base_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name="a02_vpc"
  }
}

# Create a public subnet
# To use the free tier t2.micro ec2 instance you have to declare an AZ
# Some AZs do not support this instance type
resource "aws_subnet" "a02_pub_subnet" {
  vpc_id = aws_vpc.a02_vpc.id
  cidr_block = "192.168.2.0/24"
  availability_zone = "us-west-2a"
  map_public_ip_on_launch = true

  tags={
    Name="a02_pub_subnet"
  }
}

# Create a private subnet
resource "aws_subnet" "a02_priv_subnet" {
  vpc_id            = aws_vpc.a02_vpc.id
  cidr_block        = "192.168.1.0/24"
  availability_zone = "us-west-2a"

  tags = {
    Name = "a02_priv_subnet"
  }
}

# Create two private subnets for RDS
resource "aws_subnet" "rds-sn1" {
  vpc_id            = aws_vpc.web.id
  cidr_block        = "192.168.3.0/24"
  availability_zone = "us-west-2a"

  tags = {
    Name = "rds-sn1"
  }
}

resource "aws_subnet" "rds-sn2" {
  vpc_id            = aws_vpc.web.id
  cidr_block        = "192.168.4.0/24"
  availability_zone = "us-west-2b"

  tags = {
    Name = "rds-sn2"
  }
}



# Create internet gateway for VPC
resource "aws_internet_gateway" "a02_igw" {
  vpc_id=aws_vpc.a02_vpc.id

  tags = {
    Name="a02_igw"
  }
}

# Create route table for web VPC
resource "aws_route_table" "a02_route" {
  vpc_id=aws_vpc.a02_vpc.id

  tags = {
    Name="a02_route"
  }
}

# Add route to route table
resource "aws_route" "default_route" {
  route_table_id = aws_route_table.a02_route.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.a02_igw.id
}

# Associate a02 route table with a02 subnet
resource "aws_route_table_association" "a02" {
  subnet_id = aws_subnet.a02_pub_subnet.id
  route_table_id = aws_route_table.a02_route.id
}

# create security group for the database
resource "aws_security_group" "database_security_group" {
  name        = "database security group"
  description = "enable mysql/aurora access on port 3306"
  vpc_id      = aws_vpc.web.id
  
  ingress {
    description     = "mysql access"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.web.id]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "rds_security_group"
  }
}

# create the subnet group for the rds instance
resource "aws_db_subnet_group" "database_subnet_group" {
  name        = "rds_subnet_group"
  subnet_ids  = [aws_subnet.rds-sn1.id, aws_subnet.rds-sn2.id]
  description = "Subnet group for RDS database"
  
  tags = {
    Name = "database_subnet_group"
  }
}


# Security group for EC2 instance web
resource "aws_security_group" "a02_pub_sg" {
  name="allow_ssh"
  description = "allow ssh from home and work"
  vpc_id = aws_vpc.a02_vpc.id

  tags={
    Name="a02_pub_sg"
  }
}

variable "home_cidr" {
  description = "home cidr block"
  default = "24.84.254.0/24"
}

variable "bcit_cidr" {
  description = "bcit cidr block"
  default = "142.232.0.0/16"
}

# Ingress rule for EC2 instance security group
# allow ssh
resource "aws_vpc_security_group_ingress_rule" "home_pub_ssh" {
  security_group_id = aws_security_group.a02_pub_sg.id

  cidr_ipv4 =  var.home_cidr
  from_port = 22
  ip_protocol = "tcp"
  to_port = 22
}

resource "aws_vpc_security_group_ingress_rule" "bcit_pub_ssh" {
  security_group_id = aws_security_group.a02_pub_sg.id

  cidr_ipv4 =  var.bcit_cidr
  from_port = 22
  ip_protocol = "tcp"
  to_port = 22
}

# Ingress rule for EC2 instance security group
# allow http
resource "aws_vpc_security_group_ingress_rule" "pub_http" {
  security_group_id = aws_security_group.a02_pub_sg.id

  cidr_ipv4 = "0.0.0.0/0"
  from_port = 80
  ip_protocol = "tcp"
  to_port = 80
}

resource "aws_vpc_security_group_ingress_rule" "pub_https" {
  security_group_id = aws_security_group.a02_pub_sg.id

  cidr_ipv4 = "0.0.0.0/0"
  from_port = 443
  ip_protocol = "tcp"
  to_port = 443
}

# Egress rule for EC2 instance security group
resource "aws_vpc_security_group_egress_rule" "pub_egress" {
  security_group_id=aws_security_group.a02_pub_sg.id

  cidr_ipv4 = "0.0.0.0/0"
  ip_protocol = -1
}

# Use an existing key pair on host machine with file func
# resource "aws_key_pair" "local_key"{
#   key_name = "acit_4640"
#   public_key = file("~/.ssh/acit_4640.pub")
# }

# Create EC2 instance that uses the latest Ubuntu Ami from data
# the local key above
resource "aws_instance" "a02_backend" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  # key_name = aws_key_pair.local_key.id
  key_name = "acit_4640"
  vpc_security_group_ids = [aws_security_group.a02_pub_sg.id]
  subnet_id = aws_subnet.a02_pub_subnet.id

  tags={
    Name="a02_backend"
  }
}

# create the rds instance
resource "aws_db_instance" "drwho" {
  engine                 = "mysql"
  engine_version         = "8.0.34"
  multi_az               = false
  identifier             = "rds-database-instance"
  username               = var.db_username
  password               = var.db_password
  instance_class         = "db.t3.micro"
  allocated_storage      = 10
  db_subnet_group_name   = aws_db_subnet_group.database_subnet_group.name
  vpc_security_group_ids = [aws_security_group.database_security_group.id]
  availability_zone      = "us-west-2a"
  db_name                = "drwhocompanions"
  skip_final_snapshot    = true
}



# Write data to file when resources are created
# File will be managed with terraform, deleted when resources are destroyed
resource "local_file" "vpc_vars_file" {
  content=<<-eof
    tf_vpc_id: ${aws_vpc.a02_vpc.id}
    tf_ec2_dns: ${aws_instance.a02_backend.public_dns}
  eof
  file_permission = "0640"
  filename = "vpc_vars.yaml"
}

# Print out to screen when resources are created
# also when command "terraform output" is run

# output for EC2 instance ip
output "instance_ip_addr" {
  value       = aws_instance.a02_backend.public_ip
  description = "The public IP address of the ec2 instance."
}

# outputs for RDS instance info
output "rds_hostname" {
  description = "RDS instance hostname"
  value       = aws_db_instance.drwho.address
  sensitive   = true
}

output "rds_port" {
  description = "RDS instance port"
  value       = aws_db_instance.drwho.port
  sensitive   = true
}

output "rds_username" {
  description = "RDS instance root username"
  value       = aws_db_instance.drwho.username
  sensitive   = true
}

