# Minh Tan Pham A01215507


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-west-2"
}

# Variable for vpc cidr block
variable "base_cidr_block" {
  description = "default cidr block"
  default = "10.0.0.0/16"
}

# Get the most recent ami for Ubuntu 23.04
data "aws_ami" "ubuntu" {
  most_recent = true
  owners=["099720109477"]

  filter {
    name="name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-lunar-23.04-amd64-server-*"]
  }
}

# Create a VPC
resource "aws_vpc" "web" {
  cidr_block = var.base_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name="Web"
  }
}

# Create a public subnet
# To use the free tier t2.micro ec2 instance you have to declare an AZ
# Some AZs do not support this instance type
resource "aws_subnet" "web" {
  vpc_id = aws_vpc.web.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-west-2a"
  map_public_ip_on_launch = true

  tags={
    Name="Web"
  }
}

# Create internet gateway for VPC
resource "aws_internet_gateway" "web_gw" {
  vpc_id=aws_vpc.web.id

  tags = {
    Name="Web"
  }
}

# Create route table for web VPC
resource "aws_route_table" "web" {
  vpc_id=aws_vpc.web.id

  tags = {
    Name="web-route"
  }
}

# Add route to route table
resource "aws_route" "default_route" {
  route_table_id = aws_route_table.web.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.web_gw.id
}

# Associate web route table with web subnet
resource "aws_route_table_association" "web" {
  subnet_id = aws_subnet.web.id
  route_table_id = aws_route_table.web.id
}

# Security group for EC2 instance web
resource "aws_security_group" "web" {
  name="allow_ssh"
  description = "allow ssh from home and work"
  vpc_id = aws_vpc.web.id

  tags={
    Name="Web"
  }
}

# Ingress rule for EC2 instance security group
# allow ssh
resource "aws_vpc_security_group_ingress_rule" "web-ssh" {
  security_group_id = aws_security_group.web.id

  cidr_ipv4 = "0.0.0.0/0"
  from_port = 22
  ip_protocol = "tcp"
  to_port = 22
}

# Ingress rule for EC2 instance security group
# allow http
resource "aws_vpc_security_group_ingress_rule" "web-http" {
  security_group_id = aws_security_group.web.id

  cidr_ipv4 = "0.0.0.0/0"
  from_port = 80
  ip_protocol = "tcp"
  to_port = 80
}

# Egress rule for EC2 instance security group
resource "aws_vpc_security_group_egress_rule" "web-egress" {
  security_group_id=aws_security_group.web.id

  cidr_ipv4 = "0.0.0.0/0"
  ip_protocol = -1
}

# Use an existing key pair on host machine with file func
# resource "aws_key_pair" "local_key"{
#   key_name = "acit_4640"
#   public_key = file("~/.ssh/acit_4640.pub")
# }

# Create EC2 instance that uses the latest Ubuntu Ami from data
# the local key above
resource "aws_instance" "web" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  # key_name = aws_key_pair.local_key.id
  key_name = "acit_4640"
  vpc_security_group_ids = [aws_security_group.web.id]
  subnet_id = aws_subnet.web.id

  tags={
    Name="Web"
  }
}

# Write data to file when resources are created
# File will be managed with terraform, deleted when resources are destroyed
resource "local_file" "vpc_vars_file" {
  content=<<-eof
    tf_vpc_id: ${aws_vpc.web.id}
    tf_ec2_dns: ${aws_instance.web.public_dns}
  eof
  file_permission = "0640"
  filename = "vpc_vars.yaml"
}

# Print out to screen when resources are created
# also when command "terraform output" is run
output "instance_ip_addr" {
  value = aws_instance.web.public_ip
  description = "The public IP of the EC2 instance"
}