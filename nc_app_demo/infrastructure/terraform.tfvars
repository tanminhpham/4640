# Terraform Input  Variables
# Resources:
#  - Tutorial: https: //developer.hashicorp.com/terraform/tutorials/aws-get-started/aws-variables
#  - Reference: https: //developer.hashicorp.com/terraform/language/values/variables
aws_region = "us-west-2"
project_name = "wk05"
vpc_cidr = "172.16.0.0/16"
subnet_cidr = "172.16.1.0/24"
default_route = "0.0.0.0/0"
home_net = "75.157.0.0/16"
bcit_net = "142.232.0.0/16"
ami_id = "ami-03839f1dba75bb628"
ssh_key_name = "acit_4640_202330"