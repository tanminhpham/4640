output "vpc_id" {
  value = aws_vpc.vpc_1.id
}

output "sn_1" {
  value = aws_subnet.sn_1.id

}

output "gw_1" {
  value = aws_internet_gateway.gw_1.id
}

output "rt_1" {
  value = aws_route_table.rt_1.id
}

output "sg_1" {
  value = aws_security_group.sg_1.id
}

output "ec2_instance_id" {
  value = aws_instance.ec2_instance.id
}

output "ec2_instance_public_ip" {
  value = aws_instance.ec2_instance.public_ip
}

output "ec2_instance_public_dns" {
  value = aws_instance.ec2_instance.public_dns
}
