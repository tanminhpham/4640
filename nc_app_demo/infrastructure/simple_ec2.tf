resource "aws_instance" "ec2_instance" {
  ami             = var.ami_id
  instance_type   = "t2.micro"
  key_name        = var.ssh_key_name
  subnet_id       = aws_subnet.sn_1.id
  security_groups = [aws_security_group.sg_1.id]
  tags = {
    Name    = "ec2_instance"
    Project = var.project_name
    Type = "demo"
  }
}

# https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file
resource "local_file" "inventory_file" {

  content = <<EOF
webservers:
  hosts:
    ${aws_instance.ec2_instance.public_dns}
EOF

  filename = "../service/inventory/webservers.yml"

}

# https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file
resource "local_file" "group_vars_file" {

  content = <<EOF
ec2_instance_public_dns: ${aws_instance.ec2_instance.public_dns}
EOF

  filename = "../service/group_vars/webservers.yml"

}

# https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file
resource "local_file" "script_vars_file" {

  content = <<EOF
ec2_instance_id="${aws_instance.ec2_instance.id}"
EOF

  filename = "../script_vars.sh"

}