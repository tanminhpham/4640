#! /usr/env/bin bash
set -o nounset

declare -r SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

cd "${SCRIPT_DIR}"/infrastructure || exit 1
terraform init
terraform apply -auto-approve 

cd .. || exit 1
source "${SCRIPT_DIR}/script_vars.sh"

# Hang out while the instance comes up
aws ec2 wait instance-running --instance-ids "${ec2_instance_id}"

sleep 5
cd "${SCRIPT_DIR}"/service || exit 1
ansible-playbook -i ./inventory/webservers.yml nc_server.yml